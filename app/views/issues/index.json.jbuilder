json.array!(@issues) do |issue|
  json.extract! issue, :id, :title, :status, :rank
  json.url issue_url(issue, format: :json)
end
