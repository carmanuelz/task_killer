json.array!(@todolists) do |todolist|
  json.extract! todolist, :id, :date, :notes, :result
  json.url todolist_url(todolist, format: :json)
end
