# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

current_date = moment().format 'YYYY-MM-DD'

createTaskItem = (data) ->
  li = $("<li class='list-primary ui-sortable-handle'>")
  task = $("<div class='task-title'>")
  desc = $("<span class='task-title-sp'>"+data.title+"</span>")
  contend_button = $("<div class='pull-right hidden-phone'>")
  button = $("<button class='btn-check-task btn btn-xs glyphicon glyphicon-ok'>")
  li.append task
  task.append desc
  task.append contend_button
  contend_button.append button
  return li

loadTask = (data)->
  $(data).each (index)->
    $('#sortable').append createTaskItem this    
  reloadEventsTask()

updateTaskList = (data) ->
  if(data)
    $('#sortable').empty()
    $('#task-list').show()
    $('#todolist_tools').show()
    $('#btn_modal_todolist').hide()
    $('#edit-todolist').attr 'href', gon.todolisfindbydate.replace 'select-date', current_date
    loadTask data
  else    
    $('#task-list').hide()
    $('#todolist_tools').hide()
    $('#btn_modal_todolist').show()
  
getAjaxObjectPost gon.todolisturl, {date: current_date}, updateTaskList

$('#btn_modal_todolist').click ->
  $('#todolist_form').modal 'show'

reloadEventsTask = ->
  $('.btn-check-task').hover (->
    $(this).closest('.task-title').find('.task-title-sp').addClass 'strikethrough'
    return
  ), ->
    $(this).closest('.task-title').find('.task-title-sp').removeClass 'strikethrough'
    return