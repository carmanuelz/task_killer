currentToken = null;

reloadEventsTask = ->
  $('.btn-drop-task').click ->    
    senddelete = $.ajax
      url: gon.tlitems_path+'/'+$(this).attr 'tlitem_id'
      type: 'DELETE'
    senddelete.done (data)->
      successTask data

reloadEventsReadyTask = ->
  $('.btn-list-task').click ->    
    sendlist = $.ajax
      url: $('#task-form').attr('action')
      type: 'post'
      data: { todolist_id:gon.todlist.id, task_id: $(this).attr 'task-id'}
    sendlist.done (data)->
      successList data

createReadyTaskItem = (data) ->
  li = $("<li class='list-primary ui-sortable-handle'>")
  task = $("<div class='task-title'>")
  desc = $("<span class='task-title-sp'>"+data.title+"</span>")
  contend_button = $("<div class='pull-right hidden-phone'>")
  button = $("<button class='btn-list-task btn btn-xs glyphicon glyphicon-arrow-left' task-id='"+data.id+"'>")
  li.append task
  task.append desc
  task.append contend_button
  contend_button.append button
  return li

createTaskItem = (data) ->
  li = $("<li class='list-primary ui-sortable-handle'>")
  task = $("<div class='task-title'>")
  desc = $("<span class='task-title-sp'>"+data.title+"</span>")
  contend_button = $("<div class='pull-right hidden-phone'>")
  button = $("<button class='btn-drop-task btn btn-xs glyphicon glyphicon-trash' tlitem_id='"+data.tlitem_id+"'>")
  li.append task
  task.append desc
  task.append contend_button
  contend_button.append button
  return li

loadTasks = (data)->
  $(data).each (index)->
    $('#sortable').append createTaskItem this    
  reloadEventsTask()

loadReadyTasks = (data)->
  $(data).each (index)->
    $('#ready-tasks').append createReadyTaskItem this    
  reloadEventsReadyTask()

successTask = (data) ->
  $('#sortable').empty()  
  $('#ready-tasks').empty()
  loadTasks data
  getAjaxObject gon.getoutlist_tasks_path, loadReadyTasks

successList = (data) ->
  $('#sortable').empty()
  $('#ready-tasks').empty()
  loadTasks data
  getAjaxObject gon.getoutlist_tasks_path, loadReadyTasks

loadTasks gon.tasks

$('#submit-task').click ->
  getAjaxObjectPost $('#task-form').attr('action'), $('#task-form').serializeObject(), successTask
$('#new-task-button').click ->    
  $('#ready-tasks').empty()  
  $('#ready-task-contend').hide()
  $('#task-form').show()

$('#pending-task-button').click ->  
  $('#ready-tasks').empty()
  $('#ready-task-contend').show()
  $('#task-form').hide()
  getAjaxObject gon.getoutlist_tasks_path, loadReadyTasks

$('.token-box-icon').click ->
  if currentToken != $(this).attr 'tokenname'
    $('span[tokenname="'+currentToken+'"]').toggleClass 'bg-'+ $('span[tokenname="'+currentToken+'"]').attr 'colordata'
    currentToken = $(this).attr 'tokenname'
  else    
    currentToken = null
  $(this).toggleClass 'bg-'+ $(this).attr 'colordata'