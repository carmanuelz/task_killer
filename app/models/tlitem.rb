class Tlitem < ActiveRecord::Base
	belongs_to :task
  belongs_to :todolist
  scope :with_task, -> { joins(:task).select('tlitems.id as tlitem_id, tasks.id as task_id, title') }
end
