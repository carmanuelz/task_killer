class Task < ActiveRecord::Base	
  has_many :tlitem
  enum status: [ :inlist, :pending, :finished, :outlist ]
  enum task_type: [ :shared, :personal ]
  scope :outlist, -> { where(status: 3) }
end
