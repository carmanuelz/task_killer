class Todolist < ActiveRecord::Base
	belongs_to :user
  has_many :tlitems
  has_and_belongs_to_many :tasks, :join_table => :tlitems
	enum result: [ :good, :regular, :bad ]
  enum status: [ :inprogress, :finished ]
end
