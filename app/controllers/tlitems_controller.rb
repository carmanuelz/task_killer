class TlitemsController < ApplicationController
	before_action :set_tlitem, only: [:edit, :update, :destroy]

	# POST /tasks
  # POST /tasks.json
	def create
    @todolist = Todolist.find(params[:todolist_id])
    if(params.has_key?(:task_id))
      @task = Task.find(params[:task_id])
      @task.status = 'inlist'
      @task.save
    else
      @task = current_user.tasks.new(task_params)
      @task.status = 'inlist'
      @task.task_type = 'personal'
      @task.save
    end

    @tlitem = Tlitem.new()
    @tlitem.task = @task
    @tlitem.todolist = @todolist

    if @tlitem.save
      render json: @todolist.tlitems.with_task
    else
      render json: @tlitem.errors
    end
	end

  def destroy
    @todolist = @tlitem.todolist
    @task = @tlitem.task
    @task.status = 'outlist'
    @task.save
    @tlitem.destroy
    #@responsedata = {}
    #@responsedata.listtasks = @todolist.tasks
    #@responsedata.readytasks = current_user.tasks.outlist
    render json: @todolist.tlitems.with_task
  end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_tlitem
      @tlitem = Tlitem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tlitem_params
      params.require(:tlitem).permit(:taks_id, :todolist)
    end



    # Never trust parameters from the scary internet, only allow the white list through.
    def task_params
      params.require(:task).permit(:title, :description, :date, :rank)
    end
end
