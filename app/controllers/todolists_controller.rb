class TodolistsController < ApplicationController
  before_action :set_todolist, only: [:show, :edit, :update, :destroy]
  protect_from_forgery except: :getbydate

  # GET /todolists
  # GET /todolists.json
  def index
    @todolists = Todolist.all
  end

  # GET /todolists/1
  # GET /todolists/1.json
  def show
  end

  # GET /todolists/new
  def new
    @todolist = Todolist.new
  end

  # GET /todolists/1/edit
  def edit
  end

  # POST /todolists
  # POST /todolists.json
  def create
    @todolist = current_user.todolists.new(todolist_params)

    if @todolist.save
        redirect_to addtasks_todolist_path(@todolist.id) and return
    else
        render json: @todolist.errors
    end
  end

  # PATCH/PUT /todolists/1
  # PATCH/PUT /todolists/1.json
  def update
    respond_to do |format|
      if @todolist.update(todolist_params)
        format.html { redirect_to @todolist, notice: 'Todolist was successfully updated.' }
        format.json { render :show, status: :ok, location: @todolist }
      else
        format.html { render :edit }
        format.json { render json: @todolist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /todolists/1
  # DELETE /todolists/1.json
  def destroy
    @todolist.destroy
    respond_to do |format|
      format.html { redirect_to todolists_url, notice: 'Todolist was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Custom methods

  def addtasks
    @id = params[:id]
    @todolist = Todolist.find(@id)
    gon.tasks = @todolist.tlitems.with_task
    gon.tlitems_path = tlitems_path
    gon.getoutlist_tasks_path = getoutlist_tasks_path
    gon.todlist = @todolist
  end

  def getbydate
    @todolist = current_user.todolists.find_by(date: params[:date])
    if @todolist
      render json: @todolist.tasks
    else
      render json: nil
    end
  end

  def findbydate
    @todolist = current_user.todolists.find_by(date: params[:date])
    redirect_to addtasks_todolist_path(@todolist.id) and return
  end

  # End custom methods

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todolist
      @todolist = Todolist.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todolist_params
      params.require(:todolist).permit(:date, :notes, :result)
    end
end
