namespace :update_todolist do
  desc "Actualizar todas las listas"
  task close: :environment do
    Todolist.all.each do |t|
      t.result = 'good'
      t.save
    end
    puts "#{Time.now} - Lo logramos!"
  end
end
