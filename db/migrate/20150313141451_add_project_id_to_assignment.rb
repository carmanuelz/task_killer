class AddProjectIdToAssignment < ActiveRecord::Migration
  def change
    add_reference :assignments, :project, index: true
    add_foreign_key :assignments, :projects
  end
end
