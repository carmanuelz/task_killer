class AddTodolistIdToTlitem < ActiveRecord::Migration
  def change
    add_reference :tlitems, :todolist, index: true
    add_foreign_key :tlitems, :todolists
  end
end
