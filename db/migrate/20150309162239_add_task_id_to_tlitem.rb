class AddTaskIdToTlitem < ActiveRecord::Migration
  def change
    add_reference :tlitems, :task, index: true
    add_foreign_key :tlitems, :tasks
  end
end
