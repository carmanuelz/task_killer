class AddTaskIdToTbitem < ActiveRecord::Migration
  def change
    add_reference :tbitems, :task, index: true
    add_foreign_key :tbitems, :tasks
  end
end
