class AddIssueIdToComment < ActiveRecord::Migration
  def change
    add_reference :comments, :issue, index: true
    add_foreign_key :comments, :issues
  end
end
