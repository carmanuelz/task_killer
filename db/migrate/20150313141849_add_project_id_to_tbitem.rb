class AddProjectIdToTbitem < ActiveRecord::Migration
  def change
    add_reference :tbitems, :project, index: true
    add_foreign_key :tbitems, :projects
  end
end
