class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.string :description
      t.integer :rank
      t.integer :status
      t.float :duration
      t.integer :task_type

      t.timestamps null: false
    end
  end
end
