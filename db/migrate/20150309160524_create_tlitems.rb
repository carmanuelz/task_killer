class CreateTlitems < ActiveRecord::Migration
  def change
    create_table :tlitems do |t|
    	t.string :token_assigned
      t.timestamps null: false
    end
  end
end
