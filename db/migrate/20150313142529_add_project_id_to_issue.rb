class AddProjectIdToIssue < ActiveRecord::Migration
  def change
    add_reference :issues, :project, index: true
    add_foreign_key :issues, :projects
  end
end
