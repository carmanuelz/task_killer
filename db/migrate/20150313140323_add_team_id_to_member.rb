class AddTeamIdToMember < ActiveRecord::Migration
  def change
    add_reference :members, :team, index: true
    add_foreign_key :members, :teams
  end
end
