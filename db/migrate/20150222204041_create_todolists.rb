class CreateTodolists < ActiveRecord::Migration
  def change
    create_table :todolists do |t|
      t.date :date
      t.text :notes
      t.integer :result, default: 1
      t.integer :status

      t.timestamps null: false
    end
  end
end
