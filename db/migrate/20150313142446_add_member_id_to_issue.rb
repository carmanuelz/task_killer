class AddMemberIdToIssue < ActiveRecord::Migration
  def change
    add_reference :issues, :member, index: true
    add_foreign_key :issues, :members
  end
end
