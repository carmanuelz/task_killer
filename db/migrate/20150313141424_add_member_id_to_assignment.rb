class AddMemberIdToAssignment < ActiveRecord::Migration
  def change
    add_reference :assignments, :member, index: true
    add_foreign_key :assignments, :members
  end
end
