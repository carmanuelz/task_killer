class AddUserIdToTodolist < ActiveRecord::Migration
  def change
    add_reference :todolists, :user, index: true
    add_foreign_key :todolists, :users
  end
end
