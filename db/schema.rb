# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150313170746) do

  create_table "assignments", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "member_id",  limit: 4
    t.integer  "project_id", limit: 4
  end

  add_index "assignments", ["member_id"], name: "index_assignments_on_member_id", using: :btree
  add_index "assignments", ["project_id"], name: "index_assignments_on_project_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "issue_id",   limit: 4
  end

  add_index "comments", ["issue_id"], name: "index_comments_on_issue_id", using: :btree

  create_table "issues", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "status",     limit: 4
    t.integer  "rank",       limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "member_id",  limit: 4
    t.integer  "project_id", limit: 4
  end

  add_index "issues", ["member_id"], name: "index_issues_on_member_id", using: :btree
  add_index "issues", ["project_id"], name: "index_issues_on_project_id", using: :btree

  create_table "members", force: :cascade do |t|
    t.integer  "role",       limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id",    limit: 4
    t.integer  "team_id",    limit: 4
  end

  add_index "members", ["team_id"], name: "index_members_on_team_id", using: :btree
  add_index "members", ["user_id"], name: "index_members_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.string   "title",       limit: 255
    t.string   "description", limit: 255
    t.integer  "rank",        limit: 4
    t.integer  "status",      limit: 4
    t.float    "duration",    limit: 24
    t.integer  "task_type",   limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "user_id",     limit: 4
  end

  add_index "tasks", ["user_id"], name: "index_tasks_on_user_id", using: :btree

  create_table "tbitems", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "task_id",    limit: 4
    t.integer  "project_id", limit: 4
  end

  add_index "tbitems", ["project_id"], name: "index_tbitems_on_project_id", using: :btree
  add_index "tbitems", ["task_id"], name: "index_tbitems_on_task_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.string   "descripcion", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tlitems", force: :cascade do |t|
    t.string   "token_assigned", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "todolist_id",    limit: 4
    t.integer  "task_id",        limit: 4
  end

  add_index "tlitems", ["task_id"], name: "index_tlitems_on_task_id", using: :btree
  add_index "tlitems", ["todolist_id"], name: "index_tlitems_on_todolist_id", using: :btree

  create_table "todolists", force: :cascade do |t|
    t.date     "date"
    t.text     "notes",      limit: 65535
    t.integer  "result",     limit: 4,     default: 1
    t.integer  "status",     limit: 4
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "todolists", ["user_id"], name: "index_todolists_on_user_id", using: :btree

  create_table "tokens", force: :cascade do |t|
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "tokens", ["user_id"], name: "index_tokens_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
    t.string   "time_zone",              limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "assignments", "members"
  add_foreign_key "assignments", "projects"
  add_foreign_key "comments", "issues"
  add_foreign_key "issues", "members"
  add_foreign_key "issues", "projects"
  add_foreign_key "members", "teams"
  add_foreign_key "members", "users"
  add_foreign_key "tasks", "users"
  add_foreign_key "tbitems", "projects"
  add_foreign_key "tbitems", "tasks"
  add_foreign_key "tlitems", "tasks"
  add_foreign_key "tlitems", "todolists"
  add_foreign_key "todolists", "users"
  add_foreign_key "tokens", "users"
end
